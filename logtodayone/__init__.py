# -*- coding: utf-8 -*-
"""

:copyright: © 2012, Serge Emond
:license: Apache License 2.0

"""

from __future__ import absolute_import

import os.path
import sys
import re
import subprocess

# Markdown handling
from html2md import UrlToMarkdown

# Logging
from logbook import StderrHandler, NullHandler
from logbook import Logger
from html2md.logbook import color_formatter

# Date handling
import parsedatetime.parsedatetime as pdt
from dateutil.tz import tzutc, tzlocal
from datetime import datetime


_logtodayone_text_re = re.compile(ur'\s*(?P<starred>!)?\s*(\[(?P<date>.+?)\])?\s*(?P<text>.*)')

DAYONE_PATH = '/usr/local/bin/dayone'

MARKDOWN_FORMAT = u"""
{intro}

SRC: [{url}]({url})

-----

{res[markdown]}
""".strip()


class ArgError (Exception):
    pass


def logtodayone(text):
    """Log something to dayone"""
    log = Logger('html2md')
    if not os.path.isfile(DAYONE_PATH):
        msg = "Day One's command line interface has to be installed. Please visit http://dayoneapp.com/tools/"
        log.critical(msg)
        # Also print to stdout so it gets logged in notification center if using Alfred
        print "Error: " + msg
        sys.exit(3)
    try:
        text = text.strip()

        cmd = [DAYONE_PATH]
        source = None

        m = _logtodayone_text_re.match(text)
        if m:
            if m.group('starred'):
                log.info("Starred entry")
                cmd.append("-s=true")

            date_string = m.group('date')
            if date_string:

                pieces = date_string.split(u' ')
                if pieces[0] == 'y':
                    pieces[0] = 'yesterday'
                date_string = u' '.join(pieces)

                cal = pdt.Calendar()
                parsed = cal.parse(date_string)
                if not parsed[1]:
                    raise ArgError("Can't parse date string")
                dt_local = datetime(*parsed[0][:6]).replace(tzinfo=tzlocal())
                dt_utc = dt_local.astimezone(tzutc())

                log.info("Entry date set to " + dt_local.strftime('%Y-%m-%d %H:%M:%S'))
                cmd.append("-d='{}'".format(dt_utc.strftime('%Y-%m-%d %H:%M:%S')))

            intext = m.group('text')
        else:
            intext = ''

        if not intext:
            log.info("No text, loading from clipboard")
            proc = subprocess.Popen(['/usr/bin/pbpaste'], stdout=subprocess.PIPE)
            intext, stderr = proc.communicate()
            if not intext:
                raise ArgError("Clipboard is empty")
            source = 'clipboard'
        else:
            intext = intext.strip()

        if intext.startswith('http://') or intext.startswith('https://'):
            # Import as markdown
            if ' ' in intext:
                url, intro = intext.split(' ', 1)
            else:
                url = intext
                intro = None

            u2md = UrlToMarkdown('instapaper')
            res = u2md.convert(url, simple_result=False)

            if not intro:
                intro = res['title']
            intext = MARKDOWN_FORMAT.format(url=url, intro=intro, res=res)
            source = url
        elif intext.startswith(':http://') or intext.startswith(':https://'):
            # Strip the ":"
            intext = intext[1:]

        cmd.append('new')
        proc = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        stdout, stderr = proc.communicate(intext.encode('utf-8'))
        log.debug("dayone returned {code}, {stdout!r}".format(code=proc.returncode, stdout=stdout))

        # Log some output so it displays in Alfred's Notification Center
        if source:
            print "New Day One entry from {source}".format(source=source)
        else:
            print "New Day One entry"
    except ArgError as e:
        log.error(str(e))
        sys.exit(2)


def main():
    handler = StderrHandler()
    handler.formatter = color_formatter
    handler.level = 1
    nullhandler = NullHandler()

    with nullhandler.applicationbound():
        with handler.applicationbound():
            logtodayone(u' '.join(sys.argv[1:]))


if __name__ == '__main__':
    main()
