# logtodayone

`logtodayone` is a Python script that inserts an entry into the [Day One](http://dayoneapp.com/) Mac Application.

It can also download an Instapaper mobilized version of a web page and convert it to dayone prior to insert it into dayone.

If no text is specified, it uses the current clipboard.


## Usage

`logtodayone` takes a single argument: a string of text.

That string has the following format:

    ! [date string] text|url


### Starred

If the the argument begins with a “!”, then it is starred. Else… it is not.

### Date string

The date string is parsed using [parsedatetime](https://github.com/bear/parsedatetime) because the parsing seems more natural to me than Day One's parsing.

If the date string starts with "y", it is converted to "yesterday".

You can specify things like:

 -  yesterday 9am
 -  -3d

If a date string is present, but no time is given, then it uses the current hour, with the minutes and seconds zeroed.

### URL

If the text starts with “http://” or “https://”, then it is an URL.

The URL is downloaded through Instapaper’s mobilizer, then converted to Markdown using [html2text](https://github.com/aaronsw/html2text).

The title/intro of the document it the remote page’s title. If you add text after the URL, then it’ll be used instead.

To insert the URL as-is, without anything, prefix it with “:” (e.g. “:http://google.com/”).


## Examples

`[yesterday] text`  
If it's currently 9:34, create en entry yesterday at 9am containint “text”

`![-2d 8pm] http://google.ca/`  
Create a *starred* entry two days ago, at 8pm. Google’s homepage will be downloaded and converted to markdown.


## Installation

First, make sure to install [Day One’s CLI utility](http://dayoneapp.com/tools/).

If you have `pip` installed:

    pip install logtodayone

Else,

    easy_install logtodayone


## Installation into Alfred

To be able to type "lg text" to log an entry containing "text" using Alfred:

1. go to Alfred’s preferences in the *Extensions* section
2. add a new “Shell script”, give it a name
3. in the “keyword” field, enter “lg”. Keep “silent” checked.
4. Click “Advanced”, then check “Output to Notification Center” if you want to be notified once the entry is added
5. In the “Command” text area, enter:

    logtodayone {query}
