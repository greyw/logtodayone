# -*- coding: utf-8 -*-
"""

:copyright: © 2012, Serge Emond
:license: Apache License 2.0

"""

from __future__ import absolute_import

from setuptools import setup, find_packages
from version import get_git_version

setup(
    name='logtodayone',
    version=get_git_version(),
    author=u'Serge Émond',
    author_email='greyl@greyworld.net',
    url='https://bitbucket.org/greyw/logtodayone',
    description="Script to insert text and markdown-converted web pages into Day One",
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Environment :: MacOS X',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: MacOS :: MacOS X',
        'Topic :: Utilities',
    ],
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'verlib',
        'logbook',
        'html2md',
        'parsedatetime',
        'python-dateutil',
    ],
    entry_points="""
        [console_scripts]
        logtodayone=logtodayone:main
    """,
)
